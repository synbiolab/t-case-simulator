# T-CASE: Targeted CAS Sequencing Enrichment - Simulator

T-CASE is a library preparation methodology for the study of un-localized fragments and the detection and study of their surroundings in the genome. 
Analysis pipelines have been implemented with Nextflow to analyze and simulate T-CASE data.

* T-CASE:Simulator: This pipeline allows the simulation of T-CASE library preparation and sequencing experimental process from an initial data set of sequences.
* T-CASE:Analyzer: This pipeline allows the automated analysis of the sequencing data from T-CASE library preparation.

To use [T-CASE:Analyzer](https://bitbucket.org/synbiolab/t-case-analyzer/src) please refer to Bitbucket repository linked.

## Getting Started

### Prerequisites

To be able to use T-CASE pipeline you will need to install [Nextflow](https://www.nextflow.io/), please follow [Installation instructions](https://www.nextflow.io/docs/latest/getstarted.html#installation).

A [Docker](https://www.docker.com/) container is proveded in order to run T-CASE avoiding the need of installing programes and libraries on your PC, in order to use it, please [install Docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/).

## Running the example

In order to run the program with the provided example data pull the Docker image to your local machine:

```bash
docker pull synbio/t-case:simulator
```

And use the online Bitbucket repository [1] or clone the repository to your local machine [2]:

[1]
```bash
nextflow pull https://bitbucket.org/synbiolab/t-case-simulator
nextflow run https://bitbucket.org/synbiolab/t-case-simulator
```
[2]
```bash
git clone https://Julia_MP@bitbucket.org/synbiolab/t-case-simulator.git
cd t-case-simulator
nextflow run main.nf
```

## Deployment

In order to run the program with your own data you have two options:

* Modify nextflow.config file and run normally
* Create a new nextflow2.config file adding your own data and run with the following command.

```bash
nextflow -C nextflow2.config run https://bitbucket.org/synbiolab/t-case-simulator # to substitute provided nextflow.config file
nextflow -c nextflow2.config run https://bitbucket.org/synbiolab/t-case-simulator # to merge both config files
```

If you wish to run the pipeline with Singularity instead of Docker, please pull the Docker image and create a Singularity one.

```bash
singularity pull 't-case_simulator.sif' docker://synbio/t-case:simulator
```

Create a nextflow_singularity.config file including the following information.

```groovy
docker.enabled = false
singularity.enabled = true
singularity.autoMounts = true
process.container = 't-case_simulator.sif'
```

And run normally merging both configuration files.

```bash
nextflow -c nextflow_singularity.config run https://bitbucket.org/synbiolab/t-case-simulator
```

## Output

Nextflow generates a directory named Output where fasta files containing simulated reads are stored.

### Configuration file parameters

* cores:  Maximum number of cores to use when parallelizing (set to 1 if no parallelization required).
* input: Path to the initial sequences fasta file from which the data will be simulated.
* enzymes: Path to the fasta file containing the restriction enzymes target site to cut the reference sequences. 
* guideRNA: List with as many gRNAs to use to cut input sequences, they will be processed separately. Required information per gRNA is: name, sequence, PAM, mismatch, PAM size, gRNA size, PAM pattern, PAM mismatch, PAM location (3prime or 5prime).
* maxSize: Maximum size from which the resulting digested sequences will be selected.
* minSize: Minimum size from which the resulting digested sequences will be selected.
* enzCut: Position of the cut for each restriction enzyme provided in the fasta file, using the same order.
* casCut: Position of the cut for sgRNAs.
* substitution: Path to the substitution probability matrix file to add mutation.
* deletion: Percentage of the probability of deletion to add mutation.
* insertion: Percentage of the probability of insertion to add mutation.

## Authors

* **Júlia Mir Pedrol** - *Initial work*
