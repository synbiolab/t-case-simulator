#!/usr/bin/env nextflow

process obtainCuts {
	input:
	file(input) from file(params.input)
	set val(name), val(guide), val(PAM), val(mismatch), val(PAMsize), val(gRNAsize), val(pat), val(PAMmism), val(PAMloc) from params.guideRNA
	file(enzFiles) from file(params.enzymes)
	val(cores) from params.cores

	output:
	file("${name}.RData") into R_out

	script:
	"""
	FindCuts.R $input ${name}.RData $guide $PAM $mismatch $PAMsize $gRNAsize $pat $PAMmism $PAMloc $enzFiles $cores
	"""
}

process obtainSimulatedFragments {
	publishDir "Output", mode: 'copy'	

	input:
	file(input) from file(params.input)
	file(cuts) from R_out
	val max from params.maxSize
	val min from params.minSize
	val enzCuts from params.enzCut
	val cas	from params.casCut
	file(s) from file(params.substitution)
	val d from params.deletion
	val i from params.insertion

	output:
	file("simulated_${cuts.baseName}_${input}")

	script:	
	"""
	cutGenome.py -ma $max -mi $min -rd $cuts -c $cas -s $s -d $d -i $i $input simulated_${cuts.baseName}_${input} $enzCuts
	"""	
}

